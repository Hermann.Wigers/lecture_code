package lecture6;

public class UseLibrary {

	public static void main(String[] args) {
		Book inf102Book = new Book("Robert",376,"Algorithms");
		Book myThesis = new Book("Martin", 210, "New width parameters");
		
		Library martinsShelf = new Library();
		martinsShelf.add(inf102Book);
		martinsShelf.add(myThesis);
		
		System.out.println(martinsShelf.hasBook(inf102Book));
		
		Book myWish = new Book("Martin", 210, "New width parameters");

		//this returns false because we do not have .equals method
		System.out.println(martinsShelf.hasBook(myWish));
		

	}

}
