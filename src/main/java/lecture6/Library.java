package lecture6;

import java.util.ArrayList;

public class Library {

	ArrayList<Book> books;
	
	public Library() {
		books = new ArrayList<>();
	}
	
	public void add(Book book) {
		books.add(book);
	}
	
	public boolean hasBook(Book book) {
		return books.contains(book);
	}
}
