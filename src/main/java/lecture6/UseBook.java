package lecture6;

public class UseBook {

	public static void main(String[] args) {
		Book inf102Book = new Book("Robert",376,"Algorithms");
		System.out.println("Author: "+inf102Book.getFirstAuthor());
		Book myThesis = new Book("Martin", 210, "New width parameters");
		System.out.println("Author: "+myThesis.getFirstAuthor());

		Book myFavorite = myThesis;
		System.out.println("Pages favorite: "+myFavorite.numPages);

		System.out.println("use == "+(myFavorite == myThesis));
		Book myThesis2 = new Book("Martin", 210, "New width parameters");
		System.out.println("use == "+(myThesis == myThesis2));

		System.out.println("Pages: "+myThesis.numPages);
		myThesis.tearOutPage();
		System.out.println("Pages: "+myThesis.numPages);
		System.out.println("Pages: "+inf102Book.numPages);

		System.out.println("Pages favorite: "+myFavorite.numPages);

		String textRepresentation = inf102Book.toString();
		System.out.println(textRepresentation);
		
		System.out.println(myThesis);
	}

}
